// Здесь будут находиться методы, которые подразумевают доступ к данным.
// Другой вариант - использовать $resource
// Тут уж как пожелаете.) Если пожелаете, конечно.

function HttpServiceProxy(httpService, jsonFolderPath){
    this._httpService = httpService;
    this._jsonFolderPath = jsonFolderPath;

    this._types = [];
    this.getTypes();

    // Следует понимать, что все коллекции, находящиеся в этом объекте, на самом деле находятся на сервере.
    // И подход, который применен здесь к хранению объектов не совсем правилен
}

HttpServiceProxy.prototype.getTypes = function(){
    // Здесь должен находиться следующий код:
    //    return this._httpService.get('/url/for/get/types', {params:{param1:1}})
    // И в дальнейшем использоваться промисы, но здесь их не будет))
    if (!this._types || !this._types.length){
        this._types = [
            {
                "id":1,
                "name": "Строка",
                "control": "text",
                "isComplex": false
            },
            {
                "id":2,
                "name": "Число",
                "control": "text",
                "isComplex": false
            },
            {
                "id":3,
                "name": "Галочка",
                "control": "checkbox",
                "isComplex": false
            },
            {
                "id":4,
                "name": "Сложный тип",
                "control": "select",
                "isComplex": true
            }
        ];
    }

    return this._types;
};

HttpServiceProxy.prototype.getType = function(id){
    var types = this.getTypes();
    return _.find(types, function(t){return t.id === id});
};




angular
    .module('gta.services')
    .value('gta.services.jsonFolderPath', '/TemplateAdmin/json/')
    .service('gta.services.httpServiceProxy',['$http', 'gta.services.jsonFolderPath', function(httpService, jsonFolderPath){
        return new HttpServiceProxy(httpService, jsonFolderPath);
    }]);