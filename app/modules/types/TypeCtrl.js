function TypeCtrl(httpServiceProxy, location){
    this._httpServiceProxy = httpServiceProxy;
    this._location = location;
    this.types = [];

    this.getAll();
}

TypeCtrl.prototype.getAll = function(){
    var self = this;
    self.types = self._httpServiceProxy.getTypes();
};

TypeCtrl.prototype.editType = function(id){
    if (!_.contains(function(e){return e.id === id})){
        alert('type is missing!');
        this._location.path('/types/all')
    }

    this._location.path('/types/edit/'+id);
};

angular.module('gta.types')
    .controller('gta.types.TypeCtrl', [
       'gta.services.httpServiceProxy',
        '$location',
        TypeCtrl
    ]);