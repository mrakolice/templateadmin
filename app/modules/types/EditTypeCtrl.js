function EditTypeCtrl(httpServiceProxy, routeParams){
    this._httpServiceProxy =  httpServiceProxy;
    this.getType(parseInt(routeParams.id));

    this.controls = [
        'text',
        'number',
        'checkbox',
        'select'
    ]
}

EditTypeCtrl.prototype.getType = function(id){
    this.current = this._httpServiceProxy.getType(id);
};


angular.module('gta.types')
    .controller('gta.types.EditTypeCtrl',['gta.services.httpServiceProxy', '$routeParams', EditTypeCtrl]);