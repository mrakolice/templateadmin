// Инициализация всех модулей, чтобы можно было потом к ним обращаться

//gta == Generator Template Adminka
angular.module('gta.services',[]);
angular.module('gta.types',['gta.services']);

angular.module('gta.externalDependencies',['ngRoute', 'ngSanitize','ui.select', "ui.bootstrap"]);


var app = angular.module('gta', ['gta.externalDependencies','gta.types']);

app.config(['$routeProvider', function(rp){
    rp
        .when('/entities', {
            templateUrl: 'views/entities.html'
        })
        .when('/templates', {
            templateUrl: 'views/templates.html'
        })
        .when('/types', {
            templateUrl: 'views/types/all.html'
        })
        .when('/types/edit/:id', {
            templateUrl: 'views/types/edit.html'
        })
    ;
}]);